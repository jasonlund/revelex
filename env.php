<?php
/*
 * Environment file.
 *
 * Please define all of the below.
 */

define("FILE_PATH", "/home/vagrant/Code/revelex/");

define("SALT", 'dCr}r"%4?]s<p3@y');

define("MYSQL_HOST", "localhost");
define("MYSQL_USER", "homestead");
define("MYSQL_PASSWORD", "secret");
define("MYSQL_DB", "revelex");