# Simple PHP 5 Website with Auth

## Overview
Created for a prospective job opportunity, this is a very basic PHP web application that has a user registration
and auth system. PHP5, MySQL, HTML and Twitter Bootstrap are utilized.

## Usage
Define your environment's variables in the env.php file. Once completed, point your httpd to /public.

## Components

### Auth
#### src/auth.php
Uses a MySQL table to store and recall registered users. A simple md5 hash with a defined salt stores the passwords in
non plaintext. Login uses the same hash/salt logic to ensure passwords are correct. If a user is logged in, their details
gathered at registration are displayed on the index.

### Custom Routing
#### src/routes.php
A custom .htaccess file redirects all traffic to public/index.php . src/routes.php then handles the parsing of the
current uri, verification that it is a valid uri, verification that the restfulness is correct and then calls the proper
class to handle the request.

### Template Form
#### src/html/auth-form.php
The login and register forms are handled in the same file. Fields are populated based on the current route.

## Miscellaneous
Realization time of this project was roughly 3 non-contiguous hours. No further development will be made unless
requested. Please feel free to [email](mailto:jasonlund@gmail.com) me with any questions.

A MySQL schema (schema.sql) has been provided in the root directory to create the user table.