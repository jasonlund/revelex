<?php

/*
 * First we run the boot sequence. This will include all of our necessary files and classes as well as do some auth
 * checks.
 */
require_once '../src/boot.php';

/*
 * Boot the routes loader. This will catch the current URI and direct to the proper function.
 */
require_once '../src/routes.php';

/*
 * And we're done.
 */
die();