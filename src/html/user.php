<?php
/*
 * User information. This is shown only if the user is logged in.
 */
?>

<?php if(isset($_SESSION['logged_in'])){ ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                You are logged in!
                <ul>
                    <li>
                        Name: <?php echo USER_NAME; ?>
                    </li>
                    <li>
                        Email: <?php echo USER_EMAIL; ?>
                    </li>
                    <li>
                        DOB: <?php echo USER_DOB; ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?php } ?>