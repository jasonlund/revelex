<?php
/*
 * Main callout panel. This is displayed on every page.
 */
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h2>Hello World</h2>
                <p class="lead">
                    I made this website using just PHP 5 and MySQL with some HTML and Twitter Bootstrap to make it look
                    presentable.
                </p>
                <p>
                    You can do one of the following:
                </p>
                <div class="btn-group" role="group">
                    <a href="/signup" class="btn btn-default">Sign Up</a>
                    <a href="/signin" class="btn btn-default">Sign In</a>
                    <a href="/signout" class="btn btn-default">Sign Out</a>
                    <a href="/" class="btn btn-default">Go Home</a>
                </div>
            </div>
        </div>
    </div>