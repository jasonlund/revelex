<?php
/*
 * Used for both registering and signing in a user.
 */
?>

<div class="row">
    <div class="col-lg-12">
        <form action="<?php echo ROUTE == 'Auth.register' ? '/register' : '/login'; ?>" method="post">
            <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
            </div>
            <?php if(ROUTE == 'Auth.register'): ?>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password Confirmation</label>
                    <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Password Confirmation" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Your Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" required>
                </div>
                <div class="form-inline">
                    <div class="form-group">
                        <label>Birth Month</label>
                        <input type="number" min="1" max="12" step="1" class="form-control" name="birth_month" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Birth Day</label>
                        <input type="number" min="1" max="31" step="1" class="form-control" name="birth_day" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Birth Year</label>
                        <input type="number" min="1900" max="2016" step="1" class="form-control" name="birth_year" required>
                    </div>
                </div>
            <?php endif; ?>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>