<?php

/**
 * Class Auth
 *
 * The Auth class handles user registration and authentication.
 */

class Auth
{
    /**
     * Display the registration form.
     */
    public function register()
    {
        if(isset($_SESSION['logged_in'])){
            $this->error('You are already logged in. Please log out to continue.');
        }else{
            readfile(FILE_PATH . 'src/html/index.php');
            include FILE_PATH . 'src/html/auth-form.php';
        }
    }

    /**
     * Create the user if request is valid.
     */
    public function create()
    {
        if(trim($_REQUEST['password']) != trim($_REQUEST['password_confirm']))
        {
            $this->error('Your Passwords do not match.');
        }

        $existing = (new Helper())->mysql_select('users', ['email', '=', $_REQUEST['email']]);

        if($existing != false){
            $this->error('An account already exists using that email.');
        }

        if((int)$_REQUEST['birth_month'] < 10){
            $_REQUEST['birth_month'] = '0' . $_REQUEST['birth_month'];
        }

        if((int)$_REQUEST['birth_day'] < 10){
            $_REQUEST['birth_day'] = '0' . $_REQUEST['birth_day'];
        }

        (new Helper())->mysql_insert('users', [
            'name' => $_REQUEST['name'],
            'email' => $_REQUEST['email'],
            'dob' => $_REQUEST['birth_year'] . '-' . $_REQUEST['birth_month'] . '-' . $_REQUEST['birth_day'],
            'password' => md5($_REQUEST['password'] . SALT)
        ]);

        $this->success('You have successfully created your account. You may now login. ');
    }

    /**
     * Display the use sign in form.
     */
    public function signin()
    {
        if(isset($_SESSION['logged_in'])){
            $this->error('You are already logged in. Please log out to continue.');
        }else{
            readfile(FILE_PATH . 'src/html/index.php');
            include FILE_PATH . 'src/html/auth-form.php';
        }
    }

    /**
     * Log the user in if the request is valid and the password hashes match.
     */
    public function login()
    {
        if(isset($_SESSION['logged_in'])){
            $this->error('You are already logged in. Please log out to continue.');
        }

        $existing = (new Helper())->mysql_select('users', ['email', '=', $_REQUEST['email']]);

        if($existing == false){
            $this->error('No account found using that email.');
        }

        if(md5($_REQUEST['password'] . SALT) !== $existing[0]['password']){
            $this->error('Incorrect password.');
        }

        $_SESSION['logged_in'] = $existing[0]['email'];

        $this->success('You are now logged in.');
    }

    /**
     * Log the user out if they are logged in.
     */
    public function logout()
    {
        if(! isset($_SESSION['logged_in'])){
            $this->error('You are not currently logged in.');
        }

        unset($_SESSION['logged_in']);

        $this->success('You are now logged out.');
    }

    /**
     * Check for a logged in user and define constants with their information. This is run during boot sequence.
     */
    public function check()
    {
        if(isset($_SESSION['logged_in'])){
            $existing = (new Helper())->mysql_select('users', ['email', '=', $_SESSION['logged_in']]);
            $existing = $existing[0];

            define('USER_NAME', $existing['name']);
            define('USER_EMAIL', $existing['email']);
            define('USER_DOB', $existing['dob']);
        }else{
            define('USER_NAME', '');
            define('USER_EMAIL', '');
            define('USER_DOB', '');
        }
    }

    /**
     * @param $message
     *
     * Display an error message and die.
     */
    private function error($message)
    {
        readfile(FILE_PATH . 'src/html/index.php');
        print_r('<div class="row"><div class="col-lg-12"><div class="alert alert-danger">' . $message . ' Please <a href="/">try again</a>.</div>');
        readfile(FILE_PATH . 'src/html/footer.php');
        die();
    }

    /**
     * @param $message
     *
     * Display a success message and die.
     */
    private function success($message)
    {
        readfile(FILE_PATH . 'src/html/index.php');
        print_r('<div class="row"><div class="col-lg-12"><div class="alert alert-success">' . $message . ' Please <a href="/">return home</a>.</div>');
        readfile(FILE_PATH . 'src/html/footer.php');
        die();
    }
}