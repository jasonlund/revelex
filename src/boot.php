<?php
/*
 * Bring in the environment variables.
 */
require_once '../env.php';

/*
 * Include all necessary classes.
 */
require_once 'root.php';
require_once 'auth.php';
require_once 'helper.php';

/*
 * Start the PHP session and check for auth.
 */
session_start();

(new Auth())->check();