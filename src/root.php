<?php

/**
 * Class Root
 *
 * The main index page.
 */

class Root
{
    public function index()
    {
        include FILE_PATH . 'src/html/index.php';
        include FILE_PATH . 'src/html/user.php';
    }
}