<?php

/**
 * Class Helper
 *
 * This is a general helper class to be used throughout the app.
 */
class Helper
{
    /**
     * @return mysqli
     *
     * Create a mysql connection using the environment values.
     */
    public function mysql_conn()
    {
        return new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB);
    }

    /**
     * @param $table
     * @param $attributes
     * @return bool
     *
     * Create a new row with the defined attributes in the defined table.
     */
    public function mysql_insert($table, $attributes)
    {
        $conn = $this->mysql_conn();

        $query = 'INSERT INTO ' . $table . ' (';

        foreach($attributes as $key => $value)
        {
            $query = $query . $key . ', ';
        }

        $query = substr($query, 0, -2);
        $query = $query . ') VALUES (';

        foreach($attributes as $key => $value){
            $query = $query . "'$value', ";
        }

        $query = substr($query, 0, -2);
        $query = $query . ')';

        if ($conn->query($query) === TRUE) {
            $conn->close();
            return true;
        } else {
            $conn->close();
            return false;
        }
    }

    /**
     * @param $table
     * @param $where
     * @return array|bool
     *
     * Select all rows in the given table for the given where clause. Returns false if nothing was found.
     */
    public function mysql_select($table, $where)
    {
        $conn = $this->mysql_conn();

        $query = 'SELECT * FROM ' . $table . ' WHERE ' . $where[0] . ' ' .$where[1] . "'" . $where[2] . "'";
        $result = $conn->query($query);

        if ($result->num_rows > 0){
            $data = [];
            while($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        }else{
            return false;
        }
    }
}