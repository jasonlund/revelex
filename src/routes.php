<?php

/**
 * Routing
 *
 * This is the main routing file. It directs valid routes, as defined in the $routes array, and stops invalid routes.
 */

/*
 * Define routes using the array below.
 */
$routes = [
    '/' => [
        'rest' => 'GET',
        'class' => 'Root',
        'function' => 'index'
    ],
    '/signup' => [
        'rest' => 'GET',
        'class' => 'Auth',
        'function' => 'register'
    ],
    '/register' => [
        'rest' => 'POST',
        'class' => 'Auth',
        'function' => 'create'
    ],
    '/signin' => [
        'rest' => 'GET',
        'class' => 'Auth',
        'function' => 'signin'
    ],
    '/login' => [
        'rest' => 'POST',
        'class' => 'Auth',
        'function' => 'login'
    ],
    '/signout' => [
        'rest' => 'GET',
        'class' => 'Auth',
        'function' => 'logout'
    ]
];



/*
 * Catch the uri value, and check if it is a valie route.
 */
$uri = $_SERVER['REQUEST_URI'];

if(! array_key_exists($uri, $routes)){
    four_oh_four();
}else{
    // Check the restfulness of the request.
    if((count($_REQUEST)) && ($routes[$uri]['rest'] !== 'POST')){
        four_oh_four();
    }else{
        /*
         * Valid request. We'll define a constant to know what route we're in, display the html template and run the
         * proper function to get this started.
         */

        define("ROUTE", $routes[$uri]['class'] . '.' . $routes[$uri]['function']);

        readfile(FILE_PATH . 'src/html/header.php');

        $class = new $routes[$uri]['class']();

        $class->$routes[$uri]['function']();

        readfile(FILE_PATH . 'src/html/footer.php');
    }
}

/*
 * Displays a simple 404 for invalid routes.
 */
function four_oh_four()
{
    header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
    print_r('Page Not Found');
    die();
}
